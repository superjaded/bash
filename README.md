## superjaded's bash scripts

These are mostly just random bash scripts that I've written over the years that will probably not end
up being useful to anyone else.

### clip.sh

This script just builds a command line to pass to ffmpeg to take a longer movie and cut it up into a 
clip. It uses ffmpeg's "copy" functionality, so this will have limited functionality if you want to 
actually do any transcoding.

    clip.sh <-i "inputfile.mp4" -o "outputfile.mp4"> [-f <timestamp> -t <timestamp>]

* -i = input file
* -o = output file
* -f = from time. This is literally just the -ss parameter passed to ffmpeg. This is where you want the clip to start.
* -t = to time. This is just the -to parameter passed to ffmpeg. This is where you want the clip to end

-i and -o are both required parameters, but either the f or t parameter may be omitted but will probably
have strange behavior if both are omitted. If f is omitted, the clip will start at the beginning, and
the clip will go until the end of the inputfile if t is omitted.

### combined-size.sh

Quick script that uses du to calculate the size of the directories provided. Once the size is calculated
for each of the provided directories, it adds them up and then divides them by 1024 / 1024 to get the
size in GB.

### mpv-shuffle

A set of scripts that are meant to be used through the user's file manager of choice. When you use one
of those "apps" against a folder, it will generate a playlist for mpv to use which is called with the
shuffle parameter so you get a random video.

### ssh-agent-autostart

A scriptblock that will attempt to persist the same ssh-agent to be usable through multiple ssh sessions. Originally made to be run via .bash_profile to be used when connecting via remote ssh. Has not been tested with Xorg or Wayland, but I don't see a reason to believe it wouldn't work.
