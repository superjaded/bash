#!/bin/bash

#IFS=$'\n'

totalSize=0

for item in "$@";do
    itemsize=$(du -d 0 "${item}"|cut -d$'\t' -f1)
    #echo $itemsize
    totalSize=$(expr ${totalSize} + ${itemsize})
done
echo $(expr $totalSize / 1024 / 1024)
