#!/bin/bash
# call mpv 

DIR=`dirname $0`
ID=`date +'%Y%m%d%I%M%S'`
OUTPUT="${DIR}/${ID}temp-playlist"
rm -f $OUTPUT
IFS=$'\n'
for item in $@;do 
	if [ -f $item ];then
		echo $item >> $OUTPUT
	elif [ -d $item ];then
		find "$item" -name "*" -type f >> $OUTPUT
	fi
done 
mpv --no-terminal --loop-playlist --shuffle --playlist=${OUTPUT}
rm $OUTPUT
