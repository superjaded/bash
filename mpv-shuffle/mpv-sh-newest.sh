#!/bin/sh
# call mpv 

NUMBERTORETURN=50
DIR=`dirname $0`
ID=`date +'%Y%m%d%I%M%S'`
OUTPUT="${DIR}/${ID}temp-playlist"
rm -f $OUTPUT
IFS=(printf "\n")
for item in $@;do 
	if [ -f $item ];then
		echo $item >> $OUTPUT
	elif [ -d $item ];then
		#find "$item" -name "*" -type f >> $OUTPUT
		find "$item" -name "*" -type f -printf "%T@ %p\n" | sort -r | cut -d" " -f2- | head -n $NUMBERTORETURN >> $OUTPUT
	fi
done 
cat $OUTPUT
mpv --no-terminal --loop-playlist --shuffle --playlist=${OUTPUT}
rm $OUTPUT
