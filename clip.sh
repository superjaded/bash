#!/bin/bash

while getopts ":i:o:t:f:" arg; do
        case $arg in
            i) inputfile=$OPTARG;;
            o) outputfile=$OPTARG;;
            t) endtime=$OPTARG;;
            f) starttime=$OPTARG;;
        esac
done

cmdline="ffmpeg "
if [ ! -z "$starttime" ];then
    cmdline="${cmdline} -ss ${starttime} "
fi

if [ ! -z "$endtime" ];then
    cmdline="${cmdline} -to ${endtime} "
fi

if [ ! -z "$inputfile" ];then
    cmdline="${cmdline} -i \"${inputfile}\""
else
    echo "input file missing. syntax: $0 -f starttime -t endtime -i inputfile -o outputfile"
    exit
fi

if [ ! -z "$outputfile" ];then
    cmdline="${cmdline} -c copy \"${outputfile}\""
else
    echo "Output file missing. syntax: $0 -f starttime -t endtime -i inputfile -o outputfile"
    exit
fi


echo "rencoding ${inputfile} to ${outputfile}, from ${starttime} to ${endtime}"
echo $cmdline
eval $cmdline
